from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.forms.models import model_to_dict

from django.utils import timezone

from .forms import AddTaskForm, UpdateTaskForm, RegisterForm, AddEventForm, LoginForm

from .models import ToDoItem, Event

# Create your views here.


def index(request):

    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    event_list = Event.objects.filter(user_id=request.user.id)

    # output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
    # template = loader.get_template("todolist/index.html")
    # context = {
    # 'todoitem_list': todoitem_list
    # }
    context = {
        'todoitem_list': todoitem_list,
        'event_list': event_list,
        "user": request.user
    }
    return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
    # response = "You are viewing the details of %s"
    # return HttpResponse(response % todoitem_id)
    todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    return render(request, "todolist/todoitem.html", todoitem)


def register(request):

    context = {}

    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid() == False:
            form = RegisterForm()
        else:
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email_address = form.cleaned_data['email']
            password = form.cleaned_data['password']

            # Checks if a user already exists in the database
            duplicates = User.objects.filter(username=username)

            if not duplicates:

                User.objects.create_user(username=username, first_name=first_name, last_name=last_name,
                                         email=email_address, password=password)
                return redirect("todolist:index")

            else:

                context = {
                    "error": True
                }

    return render(request, "todolist/register.html", context)


def change_password(request):

    is_user_authenticated = False

    user = authenticate(username='johndoe', password='john1234')
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username="johndoe")
        authenticated_user.set_password("john1")
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        "is_user_authenticated": is_user_authenticated,
    }
    return render(request, "todolist/change_password.html", context)


def login_view(request):

    context = {}

    if request.method == 'POST':

        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()
        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            context = {
                "username": username,
                "password": password
            }

            if user is not None:
                # Saves or creates a record in the django_session table in the database
                login(request, user)
            # redirects the user to the index.html page
                return redirect("todolist:index")
            else:
                # Provides context with error to conditionally render the error message
                context = {"error": True}
    return render(request, "todolist/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("todolist:index")


def add_task(request):

    context = {}

    if request.method == 'POST':
        form = AddTaskForm(request.POST)

        if form.is_valid() == False:
            form = AddTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            # Checks if a task already exists in the database
            duplicates = ToDoItem.objects.filter(task_name=task_name)

            if not duplicates:

                ToDoItem.objects.create(task_name=task_name, description=description,
                                        date_created=timezone.now(), user_id=request.user.id)
                return redirect("todolist:index")

            else:

                context = {
                    "error": True
                }

    return render(request, "todolist/add_task.html", context)


def update_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    context = {
        "user": request.user,
        "todoitem_id": todoitem_id,
        "task_name": todoitem[0].task_name,
        "description": todoitem[0].description,
        "status": todoitem[0].status
    }

    if request.method == 'POST':
        form = UpdateTaskForm(request.POST)

        if form.is_valid() == False:

            form = UpdateTaskForm()

        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status
                todoitem[0].save()
                return redirect("todolist:index")

            else:

                context = {
                    "error": True
                }

    return render(request, "todolist/update_task.html", context)


def delete_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()

    return redirect("todolist:index")


def add_event(request):

    context = {}

    if request.method == 'POST':
        form = AddEventForm(request.POST)

        if form.is_valid() == False:
            form = AddEventForm()
        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            # Checks if a user already has an event with the same name
            duplicates = Event.objects.filter(event_name=event_name)

            if not duplicates:

                Event.objects.create(event_name=event_name, description=description,
                                     user_id=request.user.id)
                return redirect("todolist:index")

            else:

                context = {
                    "error": True
                }

    return render(request, "todolist/add_event.html", context)


def eventitem(request, event_id):

    event = model_to_dict(Event.objects.get(pk=event_id))
    return render(request, "todolist/eventitem.html", event)
