from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.forms.models import model_to_dict

from .models import GroceryItem

# Create your views here.

# Refactor the index view to include the user logged in the session.


def index(request):
    groceryitem_list = GroceryItem.objects.all()

    context = {
        'groceryitem_list': groceryitem_list,
        'user': request.user
    }
    return render(request, "django_practice/index.html", context)


def groceryitem(request, groceryitem_id):
    # response = "You are viewing the details of %s"
    # return HttpResponse(response % groceryitem_id)
    groceryitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
    return render(request, "django_practice/groceryitem.html", groceryitem)

# Define a view for registering a user


def register(request):
    users = User.objects.all()
    is_user_registered = False
    context = {
        "is_user_registered": is_user_registered,
    }

    for indiv_user in users:
        if indiv_user.username == "johndoe":
            is_user_registered = True
            break

    if is_user_registered == False:
        user = User()
        user.username = 'johndoe'
        user.first_name = 'John'
        user.last_name = 'Doe'
        user.email = 'johndoe@mail.com'
        user.set_password('john1234')
        user.is_staff = False
        user.is_active = True
        user.save()
        context = {
            "first_name": user.first_name,
            "last_name": user.last_name,
        }

    return render(request, "django_practice/register.html", context)


# Define a view for changing the user's password.
def change_password(request):
    is_user_authenticated = False

    # authenticates the user
    # returns the user object if found and "None" if not found
    user = authenticate(username="johndoe", password="john1234")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username="johndoe")
        authenticated_user.set_password("john1")
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        "is_user_authenticated": is_user_authenticated
    }
    return render(request, "django_practice/change_password.html", context)


# Define a view for login.
def login_view(request):
    username = "johndoe"
    password = "john1234"
    user = authenticate(username=username, password=password)
    context = {
        "is_user_authenticated": False
    }
    print(user)
    if user is not None:
        # Saves creates a record in the django_session table in the database
        login(request, user)
        # redirects the user to the index.html page
        return redirect("index")
    else:
        return render(request, "django_practice/login.html", context)


# Define a view for logout.
def logout_view(request):
    logout(request)
    return redirect("index")
