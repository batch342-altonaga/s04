# django_practice/urls.py
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('groceryitem/<int:groceryitem_id>/',
         views.groceryitem, name='viewgroceryitem'),
    # Create a route for the register view
    path('register/', views.register, name='register'),
    # Create a route for the change_password view.
    path('change_password/', views.change_password, name='change_password'),
    # Create a route for the login view.
    path('login/', views.login_view, name='login'),
    # Create a route for the logout view.
    path('logout/', views.logout_view, name='logout'),
]
